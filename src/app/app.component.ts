import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';
  constructor(private router:Router){

  }

  goAbout(){
    this.router.navigate(['/about']);
  }
  goContact(){
    this.router.navigate(['/contact']);
  }
  goCoordonator(){
    this.router.navigate(['/coordonator']);
  }
  goHome() {
    this.router.navigate(['/home']);
  }
  goNoutati(){
  this.router.navigate(['/noutati']);
  }
  goProfil(){
    this.router.navigate(['/profil']);
  }
}

